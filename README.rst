================
SLEPc for Python
================

Overview
--------

Welcome to SLEPc for Python. This package provides Python bindings for
SLEPc_, the * Scalable Library for Eigenvalue Problem Computations*.

Dependencies
------------

* Python_ 2.4 to 2.7 or 3.1 to 3.4.

* A recent NumPy_ release.

* A matching version of SLEPc_  built with *shared/dynamic libraries*.

* A matching version of PETSc_  built with *shared/dynamic libraries*.

* A matching version of petsc4py_.

* To work with the in-development version, you need to install Cython_.

.. _Python:   http://www.python.org
.. _NumPy:    http://www.numpy.org
.. _SLEPc:    http://slepc.upv.es
.. _PETSc:    http://www.mcs.anl.gov/petsc/
.. _petsc4py: 
.. _Cython:   http://www.cython.org
